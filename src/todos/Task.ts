import { ISerializable } from "../ISerializable";


export class Task implements ISerializable{
    label:string;
    done:boolean = false;

    constructor(label:string){
        this.label = label;
    }
    
    toggle(): void {
        this.done = !this.done;
    }
    serialize(): object {
        let obj = {
            label: this.label,
            done: this.done,
            path: __filename,
            className: 'Task'
        }
        return obj;
        
    }
    deserialize(obj: any): void {
        this.label = obj.label;
        this.done = obj.done;
    }
}