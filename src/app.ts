
import { Dog } from "./Dog";
import { MainProcess } from "./MainProcess";
import { Task } from "./todos/Task";
import { TodoList } from "./todos/TodoList";


const mainProc = new MainProcess();

mainProc.start().then(() => {
    console.log(mainProc.instances);
})

const todoList = new TodoList();

todoList.addTask('une task');
todoList.addTask('une autre task');

mainProc.addInstance(todoList);

// mainProc.addInstance(new Task('mon label'));

// mainProc.addInstance(new Dog(1, 'fido', 'corgi', 10));

mainProc.save();