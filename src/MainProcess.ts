import * as fs from 'fs';
import { ISerializable } from './ISerializable';

export class MainProcess {
    /**
     * Le instances de classe à sérializer et déserializer 
     * au lancement de l'appli
     */
    instances:ISerializable[] = [];
    fileName = 'app-state.json'

    async start() {
        if(fs.existsSync(this.fileName)) {
            const content = fs.readFileSync(this.fileName, 'utf-8');
            const parsedContent = JSON.parse(content);
            for(const obj of parsedContent) {
                const targetClass = (await import(obj.path))[obj.className];

                const instance:ISerializable = new targetClass();
                instance.deserialize(obj);
                this.instances.push(instance);
            }
        }
    }


    addInstance(obj:ISerializable) {
        this.instances.push(obj);
    }

    save() {
        let serialized:object[] = [];
        for(const instance of this.instances) {
            serialized.push(instance.serialize());
        }
        console.log(serialized);
        fs.writeFileSync(this.fileName, JSON.stringify(serialized));
    }

}

