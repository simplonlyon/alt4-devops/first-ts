import { ISerializable } from "../ISerializable";
import { Task } from "./Task";

export class TodoList implements ISerializable{
    tasks:Task[] = [];

    addTask(label:string):void {
        const task = new Task(label);
        this.tasks.push(task);
    }

    checkTask(index:number):void {
        if(!this.tasks[index]) {
            throw new Error('No task at this index');
        }
        this.tasks[index].toggle();
    }

    clearDone():void {
        this.tasks = this.tasks.filter(task => !task.done);

        // for (let index = 0; index < this.tasks.length; index++) {
        //     const task = this.tasks[index];
        //     if(task.done) {
        //         this.tasks.splice(index, 1);
        //         index--;
        //     }
        // }
    }

    serialize(): object {
        const serializedTasks:object[] = [];
        for (const task of this.tasks) {
            serializedTasks.push(task.serialize());
        }

        return {
            path: __filename,
            className: 'TodoList',
            tasks: serializedTasks
        };
    }
    deserialize(obj: any): void {
        for(const serializedTask of obj.tasks) {
            const task = new Task('');
            task.deserialize(serializedTask);
            this.tasks.push(task);
        }
    }
}