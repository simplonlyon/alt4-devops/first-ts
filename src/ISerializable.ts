

export interface ISerializable {
    serialize():object;
    deserialize(obj:any):void;  
}